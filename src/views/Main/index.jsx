import Banner from '@/components/banner'
import styles from './styles.module.scss'
import Product from '@/components/product'


export default function Main() {
  return (
    <main className={styles.main}>
       <Banner />
       <Product />
    </main>
  )
}
