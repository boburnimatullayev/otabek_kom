"use client";

import { Container } from "@chakra-ui/react";
import cls from "./banner.module.scss";
import Slider from "react-slick";
// import "/slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Link from "next/link";

export default function Banner() {
  var settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 2000,
    autoplay: true,
    autoplaySpeed: 3000
  };

  return (
    <section className={cls.banner}>
      <Container maxW="1200px">
        <div className={cls.banner_wrap}>
          <Slider {...settings}>
            <Link href={"/"}>
              <img
                src="https://images.uzum.uz/cn9qq6hs99ouqbfv7o40/main_page_banner.jpg"
                alt="im1"
              />
            </Link>

            <Link href={"/"}>
              <img
                src="https://images.uzum.uz/cnbej7p25kub33f4ob50/main_page_banner.jpg"
                alt="im2"
              />
            </Link>

            <Link href={"/"}>
              <img
                src="https://images.uzum.uz/cn4v33h25kub33f3c6bg/main_page_banner.jpg"
                alt="im3"
              />
            </Link>
          </Slider>
        </div>
      </Container>
    </section>
  );
}
