export const data = [
  {
    id: 1,
    title:
      "Lenovo V15 / Intel Celeron N4020 / DDR4 4 GB / SSD 256Gb / 15.6 HD / Iron Grey Noutbuki",
    image:
      "https://assets.asaxiy.uz/product/items/desktop/b53b3a3d6ab90ce0268229151c9bde112022040811165745706IrxKlag5yK.png.webp",
    isDiscounts: true,
    oldPrice: 3400000,
    realPrice: 4000000,
    category: "noutbuk",
    categorySub: "Acer",
  },
  {
    id: 2,
    title:
      "Lenovo V15 / Intel Celeron N4020 / DDR4 4 GB / SSD 256Gb / 15.6 HD / Iron Grey Noutbuki",
    image:
      "https://assets.asaxiy.uz/product/items/desktop/b53b3a3d6ab90ce0268229151c9bde112022040811165745706IrxKlag5yK.png.webp",
    isDiscounts: true,
    oldPrice: 3400000,
    realPrice: 4000000,
    category: "noutbuk",
    categorySub: "Acer",
  },

  {
    id: 3,
    title:
      "Lenovo V15 / Intel Celeron N4020 / DDR4 4 GB / SSD 256Gb / 15.6 HD / Iron Grey Noutbuki",
    image:
      "https://assets.asaxiy.uz/product/items/desktop/b53b3a3d6ab90ce0268229151c9bde112022040811165745706IrxKlag5yK.png.webp",
    isDiscounts: true,
    oldPrice: 3400000,
    realPrice: 4000000,
    category: "noutbuk",
    categorySub: "Acer",
  },
  {
    id: 4,
    title:
      "Lenovo V15 / Intel Celeron N4020 / DDR4 4 GB / SSD 256Gb / 15.6 HD / Iron Grey Noutbuki",
    image:
      "https://assets.asaxiy.uz/product/items/desktop/b53b3a3d6ab90ce0268229151c9bde112022040811165745706IrxKlag5yK.png.webp",
    isDiscounts: true,
    oldPrice: 3400000,
    realPrice: 4000000,
    category: "noutbuk",
    categorySub: "Acer",
  },
  {
    id: 5,
    title: "Xiaomi Redmi 13C 8/256GB Qora Smartfoni. BESTSELLER!",
    image:
      "https://assets.asaxiy.uz/product/items/desktop/577ef1154f3240ad5b9b413aa7346a1e2023112113274758728Eaqxskyvac.png.webp",
    isDiscounts: true,
    oldPrice: 3400000,
    realPrice: 4000000,
    category: "telefon",
    categorySub: "Xiaomi",
  },
  {
    id: 6,
    title: "Xiaomi Redmi 13C 8/256GB Qora Smartfoni. BESTSELLER!",
    image:
      "https://assets.asaxiy.uz/product/items/desktop/577ef1154f3240ad5b9b413aa7346a1e2023112113274758728Eaqxskyvac.png.webp",
    isDiscounts: true,
    oldPrice: 2400000,
    realPrice: 3000000,
    category: "telefon",
    categorySub: "Xiaomi",
  },
  {
    id: 7,
    title: "Xiaomi Redmi 13C 8/256GB Qora Smartfoni. BESTSELLER!",
    image:
      "https://assets.asaxiy.uz/product/items/desktop/577ef1154f3240ad5b9b413aa7346a1e2023112113274758728Eaqxskyvac.png.webp",
    isDiscounts: true,
    oldPrice: 2400000,
    realPrice: 3000000,
    category: "telefon",
    categorySub: "Xiaomi",
  },
];
