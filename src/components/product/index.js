"use client"

import { Button, Container } from "@chakra-ui/react";
import { data } from "./mock";
import cls from "./product.module.scss";
import { NextIcon } from "../../../public/icon";

import { useRouter } from 'next/navigation'
 

export default function Product() {
  const navigate = useRouter()

  const handleClick = (id) =>{
    navigate.push(`/product/${id}`)
  }
  return (
    <section className={cls.product_wrap}>
      <Container maxW="1200px">
        <h1 className={cls.title}>
          <span>Kompyuterlar</span> <NextIcon />
        </h1>
        <div className={cls.item_container}>
          {data.map((product) =>
            product.category === `noutbuk` ? (
              <article className={cls.item} key={product.id}>
                <div className={cls.img_wrap}>
                  {product.isDiscounts ? (
                    <p className={cls.is_discount}>Chegirma</p>
                  ) : (
                    ``
                  )}
                  <img src={product.image} alt={product.title} />
                </div>
                <div className={cls.item_text_wrap}>
                  <h3 className={cls.title_item}>{product.title}</h3>
                  <p className={cls.old_price}>{product.oldPrice} so`m</p>
                  <p className={cls.real_price}>{product.realPrice} so`m</p>
                  <Button className={cls.btn} onClick={() => handleClick(product.id)}>
                     Ko`rish
                  </Button>
                </div>
              </article>
            ) : (
              <></>
            )
          )}
        </div>

        <h1 className={cls.title}>
          <span>Telefonlar</span> <NextIcon />
        </h1>
        <div className={cls.item_container}>
          {data.map((product) =>
            product.category === `telefon` ? (
              <article className={cls.item} key={product.id}>
                <div className={cls.img_wrap}>
                  {product.isDiscounts ? (
                    <p className={cls.is_discount}>Chegirma</p>
                  ) : (
                    ``
                  )}
                  <img src={product.image} alt={product.title} />
                </div>
                <div className={cls.item_text_wrap}>
                  <h3 className={cls.title_item}>{product.title}</h3>
                  <p className={cls.old_price}>{product.oldPrice} so`m</p>
                  <p className={cls.real_price}>{product.realPrice} so`m</p>
                  <Button className={cls.btn} onClick={() => handleClick(product.id)}>
                     Ko`rish
                  </Button>
                </div>
              </article>
            ) : (
              <></>
            )
          )}
        </div>
      </Container>
    </section>
  );
}
