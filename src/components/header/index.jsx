import { Box, Button, Container, IconButton } from "@chakra-ui/react";
import csl from "./styles.module.scss";
import Link from "next/link";
import { KorzinaIcon } from "../../../public/icon";

export default function Header() {
  return (
    <header className={csl.header}>
      <Container maxW="1200px">
        <Box display={`flex`} justifyContent={`space-between`}>
          <Box display={`flex`} justifyContent={`space-between`} alignItems={`center`}>
            <h2>OtabekKomp</h2>
            <nav>
              <ul>
                <li>
                  <Link className={csl.link} href={`/`}>
                    Kompyuterlar
                  </Link>
                </li>
                <li>
                  <Link className={csl.link} href={`/`}>
                    Telefonlar
                  </Link>
                </li>
              </ul>
            </nav>
          </Box>
          <Box display={`flex`} justifyContent={`center`} alignItems={`center`}>
            <Button className={csl.btn} style={{ marginRight: `10px` }}>Bog`lanish</Button>
            <div>
              <IconButton 
              colorScheme='#2E90FA;'
               icon={<KorzinaIcon />}
              />
            </div>
          </Box>
        </Box>
      </Container>
    </header>
  );
}
